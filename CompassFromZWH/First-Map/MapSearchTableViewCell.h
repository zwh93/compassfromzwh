//
//  MapSearchTableViewCell.h
//  CompassFromZWH
//
//  Created by Baletoo on 16/6/29.
//  Copyright © 2016年 zwh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapSearchTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *searchNameLab;
@property (weak, nonatomic) IBOutlet UILabel *searchAddressLab;
@property (weak, nonatomic) IBOutlet UIButton *goNavigationButton;
@end
