#JSPatch SDK

JSPatch bridges Objective-C and JavaScript using the Objective-C runtime.

##Installation

```
pod 'JSPatchSDK', '~> 1.4'
```

##Usage

Please visit [http://jspatch.com/Docs/intro](http://jspatch.com/Docs/intro).
