//
//  CustomAnnotationView.m
//  CompassFromZWH
//
//  Created by Baletoo on 16/7/7.
//  Copyright © 2016年 zwh. All rights reserved.
//

#import "CustomAnnotationView.h"

#define kCalloutWidth 150.0f
#define kCalloutHeight 50.0f

@implementation CustomAnnotationView

- (id)initWithAnnotation:(id<MAAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.bounds = CGRectMake(0, 0, 30, 30);
        self.image = [UIImage imageNamed:@"default_common_loc_logo_normal.png"];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    if (selected) {
        
        if (!self.calloutView) {
            
            self.calloutView = [[CustomCalloutView alloc] initWithFrame:CGRectMake(0, 0, kCalloutWidth, kCalloutHeight)];
            self.calloutView.center = CGPointMake(CGRectGetWidth(self.bounds) / 2.f + self.calloutOffset.x,
                                                  -CGRectGetHeight(self.calloutView.bounds) / 2.f + self.calloutOffset.y);
            
            [self addSubview:_calloutView];
        }
        else {
            
            [self addSubview:_calloutView];
        }
    }
    else {
        
        [self.calloutView removeFromSuperview];
    }
}

@end
