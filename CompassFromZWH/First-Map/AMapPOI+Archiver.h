//
//  AMapPOI+Archiver.h
//  CompassFromZWH
//
//  Created by Baletoo on 16/7/2.
//  Copyright © 2016年 zwh. All rights reserved.
//

#import <AMapSearchKit/AMapSearchKit.h>

@interface AMapPOI (Archiver) <NSCoding, NSCopying>

@end
