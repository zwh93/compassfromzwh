//
//  CustomAnnotationView.h
//  CompassFromZWH
//
//  Created by Baletoo on 16/7/7.
//  Copyright © 2016年 zwh. All rights reserved.
//

#import <MAMapKit/MAMapKit.h>
#import "CustomCalloutView.h"

@interface CustomAnnotationView : MAAnnotationView

@property (nonatomic, strong) CustomCalloutView *calloutView;

@end
