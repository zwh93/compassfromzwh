//
//  IndexViewController.m
//  CompassFromZWH
//
//  Created by Baletoo on 16/6/18.
//  Copyright © 2016年 zwh. All rights reserved.
//

#import "IndexViewController.h"
#import "MapSearchViewController.h"
#import "CustomAnnotation.h"
#import "CustomAnnotationView.h"

#define AroundRadius 5000

@interface IndexViewController () <UISearchBarDelegate, CustomMapSearchDelegate>

@property (nonatomic, strong) AMapPOI *destinationPOI;

@end

@implementation IndexViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setNavSearchBarWithFrame:CGRectMake(50, 0, DEF_SCREEN_WIDTH-100, 44)];
    [self initLeftNavigationBarWithTitle:@"导航" Color:Dark_Font_Color];
    [self initRightNavigationBarWithTitle:@"清除" Color:Dark_Font_Color];
//    [self addBottomToolBar];
}

- (void)clearAnnotations
{
    for (CustomAnnotation *annotation in self.mapView.annotations) {
        
        if (![annotation isKindOfClass:[MAUserLocation class]]) {
            
            [self.mapView removeAnnotation:annotation];
        }
    }
}

/**
 *  设置导航搜索栏
 *
 *  @param rect CGRect
 */
- (void)setNavSearchBarWithFrame:(CGRect)rect
{
    self.searchBar = [[UISearchBar alloc] initWithFrame:rect];
    self.searchBar.barStyle = UIBarStyleBlack;
    self.searchBar.placeholder = @"搜索地点或地址";
    self.searchBar.translucent = YES;
    self.searchBar.tintColor = ClearColor;
    self.searchBar.inputAccessoryView.backgroundColor = WhiteColor;
    self.searchBar.delegate = self;
    [self.searchBar setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    self.navigationItem.titleView = _searchBar;
}

/**
 *  添加底部工具栏
 *
 *  @return <#return value description#>
 */
- (void)addBottomToolBar
{
    UIToolbar *tooBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, DEF_SELF_VIEW_HEIGHT-64-44, DEF_SELF_VIEW_WIDTH, 44)];
    
    tooBar.barTintColor = WhiteColor;
    
    [self.view addSubview:tooBar];
}

/**
 *  重写父类方法 viewDidAppear中执行
 */
- (void)hookAction
{
    self.mapView.showsUserLocation = YES;
    [self.mapView setUserTrackingMode:MAUserTrackingModeFollow animated:YES];
}

/**
 *  重写父类rightBarButton绑定方法
 *
 *  @param sender <#sender description#>
 */
- (void)rightAction:(id)sender
{
    [self clearAnnotations];
    self.searchBar.text = @"";
}

#pragma mark - MapViewDelegate
- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation
{
    
    if ([annotation isKindOfClass:[CustomAnnotation class]]) {
        
        static NSString *identifier = @"customAnnotation";
        
        CustomAnnotationView *annotationView = (CustomAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        if (!annotationView) {
            
            annotationView = [[CustomAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        }
        
        return annotationView;
    }
    else if ([annotation isKindOfClass:[MAUserLocation class]]) {
        
        static NSString *identifier = @"userAnnotation";
        
        MAAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        return annotationView;
    }
    
    return nil;
}

- (IBAction)searchAround:(id)sender
{
    MapSearchViewController *searchVC = [[MapSearchViewController alloc] init];
    
    searchVC.delegate = self;
    
    BaseNavigationViewController *navigation = [[BaseNavigationViewController alloc] initWithRootViewController:searchVC];
    
    [self presentViewController:navigation animated:NO completion:nil];
}
#pragma mark - UISearchBarDelegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    MapSearchViewController *searchVC = [[MapSearchViewController alloc] init];
    
    searchVC.delegate = self;
    
    BaseNavigationViewController *navigation = [[BaseNavigationViewController alloc] initWithRootViewController:searchVC];
    
    [self presentViewController:navigation animated:NO completion:nil];

}

#pragma mark - CustomMapSearchDelegate
- (void)searchWithKeywordOrPOI:(id)item
{
    if ([item isKindOfClass:[AMapPOI class]]) {
        
        [self clearAnnotations];
        
        CustomAnnotation *annotation = [[CustomAnnotation alloc] initWithPOI:item];
        
        self.searchBar.text = [NSString stringWithFormat:@"%@ %@", annotation.title, annotation.subtitle];
        
        [self.mapView addAnnotation:annotation];
        
        self.destinationPOI = item;
        
        [self.mapView showAnnotations:self.mapView.annotations animated:YES];
    }
    else if ([item isKindOfClass:[NSString class]])
    {
        self.searchBar.text = item;
        
        [self searchPOIWithKeywords:item];
    }
}

/**
 *  根据type搜索周边
 *
 *  @return <#return value description#>
 */

- (void)searchPOIWithTypes:(NSString *)types
{
    self.searchBar.text = types;
    [self searchPOIWithKeywords:types];
}

#pragma mark - 搜索相关
/**
 *  根据key搜索地点
 *
 *  @return <#return value description#>
 */

- (void)searchPOIWithKeywords:(NSString *)keywords
{
    [self clearAnnotations];
    
    AMapPOIAroundSearchRequest *place = [[AMapPOIAroundSearchRequest alloc] init];
    place.keywords = keywords;
    place.location = [AMapGeoPoint locationWithLatitude:self.mapView.userLocation.location.coordinate.latitude longitude:self.mapView.userLocation.location.coordinate.longitude];
    place.radius = AroundRadius;
    
    [self.search AMapPOIAroundSearch:place];
}

- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response
{
    if (response) {
        
        for (AMapPOI *poi in response.pois) {
            
            CustomAnnotation *annotation = [[CustomAnnotation alloc] initWithPOI:poi];
            
            [self.mapView addAnnotation:annotation];
            
        }
        
        [self.mapView showAnnotations:self.mapView.annotations animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
