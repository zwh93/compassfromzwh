//
//  IndexViewController.h
//  CompassFromZWH
//
//  Created by Baletoo on 16/6/18.
//  Copyright © 2016年 zwh. All rights reserved.
//
#import "BaseMapViewController.h"

@interface IndexViewController : BaseMapViewController

@property (nonatomic, strong) UISearchBar *searchBar;

@end
