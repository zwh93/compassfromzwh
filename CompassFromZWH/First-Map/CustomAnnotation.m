//
//  CustomAnnotation.m
//  CompassFromZWH
//
//  Created by Baletoo on 16/7/3.
//  Copyright © 2016年 zwh. All rights reserved.
//

#import "CustomAnnotation.h"

@implementation CustomAnnotation

@synthesize poi = _poi;

#pragma mark - MAAnnotation Protocol

- (NSString *)title
{
    return self.poi.name;
}

- (NSString *)subtitle
{
    return self.poi.address;
}

- (CLLocationCoordinate2D)coordinate
{
    return CLLocationCoordinate2DMake(self.poi.location.latitude, self.poi.location.longitude);
}

#pragma mark - Life Cycle

- (id)initWithPOI:(AMapPOI *)poi
{
    if (self = [super init])
    {
        self.poi = poi;
    }
    
    return self;
}

@end
