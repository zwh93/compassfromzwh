//
//  MapSearchViewController.h
//  CompassFromZWH
//
//  Created by Baletoo on 16/6/28.
//  Copyright © 2016年 zwh. All rights reserved.
//

#import "BaseViewController.h"
#import <AMapSearchKit/AMapSearchAPI.h>

@protocol CustomMapSearchDelegate <NSObject>

- (void)searchWithKeywordOrPOI:(id)item;

- (void)searchPOIWithTypes:(NSString *)types;

@end

@interface MapSearchViewController : BaseViewController

@property (nonatomic, strong) UISearchBar *searchBar;

@property (nonatomic, strong) AMapSearchAPI *search;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *searchDataArr;

@property (nonatomic, weak) id<CustomMapSearchDelegate> delegate;

@end
