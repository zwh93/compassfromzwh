//
//  AppDelegate.m
//  CompassFromZWH
//
//  Created by Baletoo on 16/6/18.
//  Copyright © 2016年 zwh. All rights reserved.
//

#import "AppDelegate.h"
#import "IndexViewController.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <JSPatch/JSPatch.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [self doPrepare];
    
    self.window = [[UIWindow alloc] initWithFrame:DEF_SCREEN_FRAME];
    
    IndexViewController *index = [[IndexViewController alloc] init];
    
    BaseNavigationViewController *navigation = [[BaseNavigationViewController alloc] initWithRootViewController:index];
    
    self.window.rootViewController = navigation;
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)doPrepare
{
    //JSPatch
    [JSPatch testScriptInBundle];
//    [JSPatch startWithAppKey:@"12d9d8873e12d12b"];
    
    //高德地图
    [AMapServices sharedServices].apiKey = @"021bbfb9662f131678dbfd6d3d36b3cd";
    [MAMapServices sharedServices].apiKey = @"021bbfb9662f131678dbfd6d3d36b3cd";
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
