//
//  ZWHDefine.h
//  CompassFromZWH
//
//  Created by Baletoo on 16/6/20.
//  Copyright © 2016年 zwh. All rights reserved.
//

#ifndef ZWHDefine_h
#define ZWHDefine_h

/**
 *  DEBUG
 *
 *  @param view <#view description#>
 *
 *  @return <#return value description#>
 */
#ifdef DEBUG
# define DEF_DEBUG(fmt, ...) NSLog((@"\n[文件名:%s]\n" "[函数名:%s]\n" "[行号:%d] \n" fmt), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__);
#define DEF_String_VAR(VARNAME) [NSString stringWithFormat:@"%@",@#VARNAME]

#else
# define DEF_DEBUG(...) {}
#endif

/**
 *  获取版本号r
 *
 *  @param view <#view description#>
 *
 *  @return <#return value description#>
 */
#define DEF_VERSION  [NSString stringWithFormat:@"%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]]

/**
 *	获取视图宽度
 *
 *	@param 	view 	视图对象
 *
 *	@return	宽度
 */
#define DEF_WIDTH(view) view.bounds.size.width

/**
 *	获取视图高度
 *
 *	@param 	view 视图对象
 *
 *	@return	高度
 */
#define DEF_HEIGHT(view) view.bounds.size.height

/**
 *	获取视图原点横坐标
 *
 *	@param 	view 	视图对象
 *
 *	@return	原点横坐标
 */
#define DEF_LEFT(view) view.frame.origin.x

/**
 *	获取视图原点纵坐标
 *
 *	@param 	view 	视图对象
 *
 *	@return	原点纵坐标
 */
#define DEF_TOP(view) view.frame.origin.y

/**
 *	获取视图右下角横坐标
 *
 *	@param 	view 	视图对象
 *
 *	@return	右下角横坐标
 */
#define DEF_RIGHT(view) (DEF_LEFT(view) + DEF_WIDTH(view))

/**
 *	获取视图右下角纵坐标
 *
 *	@param 	view 	视图对象
 *
 *	@return	右下角纵坐标
 */
#define DEF_BOTTOM(view) (DEF_TOP(view) + DEF_HEIGHT(view))

/**
 *  主屏的宽
 */
#define DEF_SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width

/**
 *  主屏的高
 */
#define DEF_SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height

/**
 *  self.view的宽
 */
#define DEF_SELF_VIEW_WIDTH [self.view bounds].size.width

/**
 *  self.view的高
 */
#define DEF_SELF_VIEW_HEIGHT [self.view bounds].size.height

/**
 *  主屏的size
 */
#define DEF_SCREEN_SIZE   [[UIScreen mainScreen] bounds].size

/**
 *  主屏的frame
 */
#define DEF_SCREEN_FRAME  [UIScreen mainScreen].bounds

/**
 *	生成RGB颜色
 *
 *	@param 	red 	red值（0~255）
 *	@param 	green 	green值（0~255）
 *	@param 	blue 	blue值（0~255）
 *
 *	@return	UIColor对象
 */
#define DEF_RGB_COLOR(red, green, blue) [UIColor colorWithRed:(red)/255.0f green:(green)/255.0f blue:(blue)/255.0f alpha:1]

/**
 *	生成RGBA颜色
 *
 *	@param 	red 	red值（0~255）
 *	@param 	green 	green值（0~255）
 *	@param 	blue 	blue值（0~255）
 *	@param 	alpha 	blue值（0~1）
 *
 *	@return	UIColor对象
 */
#define DEF_RGBA_COLOR(r, g, b, a) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]

/**
 *  判断屏幕尺寸是否为640*1136
 *
 *	@return	判断结果（YES:是 NO:不是）
 */
#define DEF_SCREEN_IS_640_1136 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

#define WorldColor [UIColor colorWithRed:130/255.0f green:130/255.0f blue:130/255.0f alpha:1]
#define WorldValue [UIColor colorWithRed:114/255 green:114/255 blue:114/255 alpha:.2]
#define ClearColor [UIColor clearColor]
#define BlackColor [UIColor blackColor]
#define WhiteColor [UIColor whiteColor]

/**
 *  字号适配
 */
#define DEF_WIDTH_FIT (([UIScreen mainScreen].bounds.size.width / 375.0))
#define DEF_HEIGHT_FIT (([UIScreen mainScreen].bounds.size.height / 667.0))


/**
 *	永久存储对象
 *
 *	@param	object    需存储的对象
 *	@param	key    对应的key
 */
#define DEF_PERSISTENT_SET_OBJECT(object, key)                                                                                                 \
({                                                                                                                                             \
NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];                                                                         \
[defaults setObject:object forKey:key];                                                                                                    \
[defaults synchronize];                                                                                                                    \
})

/**
 *	取出永久存储的对象
 *
 *	@param	key    所需对象对应的key
 *	@return	key所对应的对象
 */
#define DEF_PERSISTENT_GET_OBJECT(key) [[NSUserDefaults standardUserDefaults] objectForKey:key]

/**
 *  移除存储的对象
 *
 *  @param a <#a description#>
 *
 *  @return <#return value description#>
 */
#define DEF_PERSISTENT_REMOVE_OBJECT(key) [[NSUserDefaults standardUserDefaults] removeObjectForKey:key]

/**
 *  主色调色值
 */
#define Main_Color(a) [UIColor colorWithRed:255/255.0 green:30/255.0 blue:0 alpha:(a)]

/**
 *  分割线色值
 */
#define Line_Color [UIColor colorWithRed:234/255.0 green:234/255.0 blue:234/255.0 alpha:1]

/**
 *  深色字体色值
 */
#define Dark_Font_Color [UIColor colorWithRed:44/255.0 green:43/255.0 blue:43/255.0 alpha:1]

/**
 *  浅色字体色值
 */
#define Light_Font_Color [UIColor colorWithRed:91/255.0 green:91/255.0 blue:91/255.0 alpha:1]

/**
 *  背景灰色
 */
#define backgroundGrayColor [UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1]

#define SearchHistoryKey @"searchHistory"

#define systemFont @"Helvetica"

#endif /* ZWHDefine_h */
