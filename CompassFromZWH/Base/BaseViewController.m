//
//  BaseViewController.m
//  CompassFromZWH
//
//  Created by Baletoo on 16/6/18.
//  Copyright © 2016年 zwh. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = WhiteColor;
}

/**
 *  设置导航栏标题
 *
 *  @param title NSString
 */
- (void)setNavTitle:(NSString *)title
{
    self.title = title;
}

/**
 *  设置左侧导航按钮
 *
 *  @param title <#title description#>
 */
- (void)initLeftNavigationBarWithTitle:(NSString *)title Color:(UIColor *)color
{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:title
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(returnAction:)];
    self.navigationItem.leftBarButtonItem.tintColor = color;
    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:systemFont size:16]} forState:UIControlStateNormal];
}

/**
 *  设置右侧导航按钮
 *
 *  @param title <#title description#>
 */
- (void)initRightNavigationBarWithTitle:(NSString *)title Color:(UIColor *)color
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:title
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(rightAction:)];
    self.navigationItem.rightBarButtonItem.tintColor = color;
    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:systemFont size:16]} forState:UIControlStateNormal];
}

#pragma mark - Left Action

- (void)returnAction:(id)sender
{
}

#pragma mark - Right Action

- (void)rightAction:(id)sender
{
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
