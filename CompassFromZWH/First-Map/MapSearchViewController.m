//
//  MapSearchViewController.m
//  CompassFromZWH
//
//  Created by Baletoo on 16/6/28.
//  Copyright © 2016年 zwh. All rights reserved.
//

#import "MapSearchViewController.h"
#import "MapSearchTableViewCell.h"

#define HeaderHeight 154.0f

#define PositionCellIdentifer @"positionCell"
#define KeywordCellIdentifer @"keywordCell"
#define PositionHistoryCellIdentifer @"positionHistoryCell"
@interface MapSearchViewController () <UISearchBarDelegate, AMapSearchDelegate, UITableViewDelegate, UITableViewDataSource>

{
    BOOL _isSearching;
}

@end

@implementation MapSearchViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.searchDataArr = [NSMutableArray arrayWithArray:[self getSearchHistory]];
    }
    
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.searchDataArr = [NSMutableArray arrayWithArray:[self getSearchHistory]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setNavSearchBarWithFrame:CGRectMake(15, 0, DEF_SCREEN_WIDTH-65, 44)];
    [self initRightNavigationBarWithTitle:@"取消" Color:Dark_Font_Color];
    [self initSearch];
}

/**
 *  设置导航搜索栏
 *
 *  @param rect CGRect
 */
- (void)setNavSearchBarWithFrame:(CGRect)rect
{
    self.searchBar = [[UISearchBar alloc] initWithFrame:rect];
    self.searchBar.barStyle = UIBarStyleBlack;
    self.searchBar.placeholder = @"搜索地点或地址";
    self.searchBar.translucent = YES;
    self.searchBar.delegate = self;
    [self.searchBar setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    self.navigationItem.hidesBackButton = YES;
    
    self.navigationItem.titleView = _searchBar;
    
    [self.searchBar becomeFirstResponder];
}

/**
 *  初始化搜索API
 */
- (void)initSearch
{
    self.search = [[AMapSearchAPI alloc] init];
    self.search.delegate = self;
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchDataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MapSearchTableViewCell *cell;
    if ([self.searchDataArr[indexPath.row] isKindOfClass:[AMapPOI class]]) {
        
        if (_isSearching) {
            
            cell = [tableView dequeueReusableCellWithIdentifier:PositionCellIdentifer];
            
            if (!cell) {
                
                cell = [[[NSBundle mainBundle] loadNibNamed:@"MapSearchTableViewCell" owner:self options:nil] firstObject];
            }
        }
        else {
            
            cell = [tableView dequeueReusableCellWithIdentifier:PositionHistoryCellIdentifer];
            
            if (!cell) {
                
                cell = [[[NSBundle mainBundle] loadNibNamed:@"MapSearchTableViewCell" owner:self options:nil] objectAtIndex:2];
            }
        }
        
        AMapPOI *poi = self.searchDataArr[indexPath.row];
        
        cell.searchNameLab.text = poi.name;
        
        cell.searchAddressLab.text = [NSString stringWithFormat:@"%@ %@",poi.city,poi.address];
    }
    else if ([self.searchDataArr[indexPath.row] isKindOfClass:[NSString class]]) {
        
        cell = [tableView dequeueReusableCellWithIdentifier:KeywordCellIdentifer];
        
        if (!cell) {
            
            cell = [[[NSBundle mainBundle] loadNibNamed:@"MapSearchTableViewCell" owner:self options:nil] objectAtIndex:1];
        }
        
        NSString *keyword = self.searchDataArr[indexPath.row];
        
        cell.searchNameLab.text = keyword;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.searchDataArr[indexPath.row] isKindOfClass:[AMapPOI class]]) {
        
        return 64.0f;
    }
    else if ([self.searchDataArr[indexPath.row] isKindOfClass:[NSString class]]) {
        
        return 44.0f;
    }
    
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (_isSearching) {
        
        return nil;
    }
    else {
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DEF_WIDTH(tableView), HeaderHeight)];
        view.backgroundColor = WhiteColor;
        
        float buttonWidth = DEF_WIDTH(tableView)/4.0;
        float buttonHeight = 43;
        NSArray *images = @[@"default_generalsearch_keyword_food.png", @"default_generalsearch_keyword_bus.png", @"default_generalsearch_keyword_bank.png", @"default_generalsearch_keyword_scen.png", @"default_generalsearch_keyword_hotel.png", @"default_generalsearch_keyword_collect.png", @"default_generalsearch_keyword_gas.png", @"default_generalsearch_keyword_scen.png"];
        NSArray *title = @[@"美食", @"交通", @"服务", @"景点", @"酒店", @"娱乐", @"加油站", @"购物"];
        
        for (int i=0; i<8; i++) {
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(buttonWidth*(i%4), 10+(buttonHeight+29)*(i/4), buttonWidth, buttonHeight);
            [button setImage:[UIImage imageNamed:images[i]] forState:UIControlStateNormal];
            button.tag = 100+i;
            [button addTarget:self action:@selector(sugestBtnSelected:) forControlEvents:UIControlEventTouchUpInside];
            
            [view addSubview:button];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(DEF_LEFT(button), DEF_BOTTOM(button)+5, buttonWidth, 14)];
            label.text = title[i];
            label.font = [UIFont fontWithName:systemFont size:12.0f];
            label.textAlignment = NSTextAlignmentCenter;
            
            [view addSubview:label];
        }
        
        return view;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (_isSearching) {
        
        return 0.5;
    }
    else {
        
        return HeaderHeight;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_isSearching) {
        
        [self addSeachHistory:self.searchDataArr[indexPath.row]];
    }
    
    [self dismissWithSearchItem:self.searchDataArr[indexPath.row]];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_isSearching) {
        
        return NO;
    }
    
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"删除";
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [self deleteSeachHistory:indexPath.row];
        [self.searchDataArr removeObjectAtIndex:indexPath.row];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    }
}

#pragma mark - serchBarDelegate
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if ([searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) {
        
        _isSearching = YES;
        self.searchDataArr = [NSMutableArray arrayWithCapacity:0];
        [self.tableView reloadData];
        
        [self searchPOIWithKeywords:searchText];
    }
    else {
        
        _isSearching = NO;
        self.searchDataArr = [NSMutableArray arrayWithArray:[self getSearchHistory]];
        [self.tableView reloadData];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    if ([searchBar.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length > 0) {
        
        [self addSeachHistory:searchBar.text];
    }
    
    [self dismissWithSearchItem:searchBar.text];
}

#pragma mark - 搜索相关
/**
 *  根据key搜索地点
 *
 *  @return <#return value description#>
 */

- (void)searchPOIWithKeywords:(NSString *)keywords
{
    AMapPOIKeywordsSearchRequest *place = [[AMapPOIKeywordsSearchRequest alloc] init];
    place.keywords = keywords;
    place.city = @"上海";
    
    [self.search AMapPOIKeywordsSearch:place];
}

- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response
{
    if (response) {
        
        self.searchDataArr = [NSMutableArray arrayWithArray:response.pois];
        [self.tableView reloadData];
    }
}

#pragma mark - 选择推荐搜索关键词
- (void)sugestBtnSelected:(UIButton *)sender
{
    NSArray *keyword = @[@"美食", @"交通", @"服务", @"景点", @"酒店", @"娱乐", @"加油站", @"购物"];
    [self dismissWithSearchTypes:keyword[sender.tag-100]];
}

#pragma mark - 选中搜索项回传
- (void)dismissWithSearchItem:(id)item
{
    [self dismissViewControllerAnimated:NO completion:^{
        
        [self.delegate searchWithKeywordOrPOI:item];
    }];
}

#pragma mark - 选中分类项回传
- (void)dismissWithSearchTypes:(NSString *)types
{
    [self dismissViewControllerAnimated:NO completion:^{
        
        [self.delegate searchPOIWithTypes:types];
    }];
}

#pragma mark - RightAction
- (void)rightAction:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - 添加搜索历史记录
- (void)addSeachHistory:(id)item
{
    NSMutableArray *history = [NSMutableArray arrayWithArray:[self getSearchHistory]];
    
    [history addObject:item];
    
    DEF_PERSISTENT_SET_OBJECT([NSKeyedArchiver archivedDataWithRootObject:history], SearchHistoryKey);
}

#pragma mark - 获取搜索历史记录
- (NSArray *)getSearchHistory
{
    NSData *data = DEF_PERSISTENT_GET_OBJECT(SearchHistoryKey);
    
    NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    return array;
}

#pragma mark - 添加搜索历史记录
- (void)deleteSeachHistory:(NSInteger)index
{
    NSMutableArray *history = [NSMutableArray arrayWithArray:[self getSearchHistory]];
    
    if (history.count > index) {
        
        [history removeObjectAtIndex:index];
    }
    
    DEF_PERSISTENT_SET_OBJECT([NSKeyedArchiver archivedDataWithRootObject:history], SearchHistoryKey);
}

#pragma mark - 清空搜索历史记录
- (void)clearSearchHistory
{
    DEF_PERSISTENT_REMOVE_OBJECT(SearchHistoryKey);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
