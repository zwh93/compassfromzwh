//
//  BaseViewController.h
//  CompassFromZWH
//
//  Created by Baletoo on 16/6/18.
//  Copyright © 2016年 zwh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

/**
 *  设置导航栏标题
 *
 *  @param title <#title description#>
 */
- (void)setNavTitle:(NSString *)title;

/**
 *  设置左侧导航按钮
 *
 *  @param title <#title description#>
 */
- (void)initLeftNavigationBarWithTitle:(NSString *)title Color:(UIColor *)color;

/**
 *  设置右侧导航按钮
 *
 *  @param title <#title description#>
 */
- (void)initRightNavigationBarWithTitle:(NSString *)title Color:(UIColor *)color;

@end
