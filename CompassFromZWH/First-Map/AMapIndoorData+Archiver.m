//
//  AMapIndoorData+Archiver.m
//  CompassFromZWH
//
//  Created by Baletoo on 16/7/2.
//  Copyright © 2016年 zwh. All rights reserved.
//

#import "AMapIndoorData+Archiver.h"
#import <objc/runtime.h>

@implementation AMapIndoorData (Archiver)

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    unsigned int count = 0;
    objc_property_t *propertys = class_copyPropertyList([self class], &count);
    
    for (int i = 0; i<count; i++) {
        
        // 取出i位置对应的成员变量
        objc_property_t property = propertys[i];
        
        // 查看成员变量
        const char *name = property_getName(property);
        
        // 归档
        NSString *key = [NSString stringWithUTF8String:name];
        
        id value = [self valueForKey:key];
        
        if (value) {
            
            [aCoder encodeObject:value forKey:key];
        }
        
    }
    
    free(propertys);
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [self init];
    if (self) {
        
        unsigned int count = 0;
        objc_property_t *propertys = class_copyPropertyList([self class], &count);
        
        for (int i = 0; i<count; i++) {
            
            // 取出i位置对应的成员变量
            objc_property_t property = propertys[i];
            
            // 查看成员变量
            const char *name = property_getName(property);
            
            // 归档
            NSString *key = [NSString stringWithUTF8String:name];
            
            id value = [aDecoder decodeObjectForKey:key];
            
            if (value) {
                
                [self setValue:value forKey:key];
            }
            
        }
        
        free(propertys);
    }
    
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    AMapIndoorData *copy = [[[self class] allocWithZone:zone] init];
    unsigned int count = 0;
    objc_property_t *propertys = class_copyPropertyList([self class], &count);
    
    for (int i = 0; i<count; i++) {
        
        // 取出i位置对应的成员变量
        objc_property_t property = propertys[i];
        
        // 查看成员变量
        const char *name = property_getName(property);
        
        // 归档
        NSString *key = [NSString stringWithUTF8String:name];
        
        id value = [[copy valueForKey:key] copyWithZone:zone];
        
        if (value) {
            
            [copy setValue:value forKey:key];
        }
        
    }
    
    free(propertys);
    
    return copy;
}

@end
